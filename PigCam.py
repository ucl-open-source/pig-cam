#!/usr/bin/env python3
import cv2
import depthai as dai
import paho.mqtt.client as mqtt
import time

### Parameters
triggerTopic = "myrpi/triggercam"
responseTopic = "myrpi/camresponse"
brokerID = "test.mosquitto.org"
brokerPort = 1883

### Setup camera
# Create pipeline
pipeline = dai.Pipeline()

# Define source and output + properties
camRgb = pipeline.create(dai.node.ColorCamera)
camRgb.setInterleaved(False)
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.RGB)

xoutRgb = pipeline.create(dai.node.XLinkOut)
xoutRgb.setStreamName("rgb")

jpeg = pipeline.create(dai.node.VideoEncoder)
jpeg.setDefaultProfilePreset(1920, 1080, camRgb.getFps(), dai.VideoEncoderProperties.Profile.MJPEG)

script = pipeline.create(dai.node.Script)
script.setProcessor(dai.ProcessorType.LEON_CSS)
script.setScript("""
    import time
    import socket
    import fcntl
    import struct
    from socketserver import ThreadingMixIn
    from http.server import BaseHTTPRequestHandler, HTTPServer

    PORT = 8080

    def get_ip_address(ifname):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            -1071617759,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15].encode())
        )[20:24])

    class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
        pass

    class HTTPHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            if self.path == '/':
                self.send_response(200)
                self.end_headers()
                self.wfile.write(b'<h1>[DepthAI] Hello, world!</h1><p>Click <a href="img">here</a> for an image</p>')
            elif self.path == '/img':
                try:
                    self.send_response(200)
                    self.send_header('Content-type', 'multipart/x-mixed-replace; boundary=--jpgboundary')
                    self.end_headers()
                    fpsCounter = 0
                    timeCounter = time.time()
                    while True:
                        jpegImage = node.io['jpeg'].get()
                        self.wfile.write("--jpgboundary".encode())
                        self.wfile.write(bytes([13, 10]))
                        self.send_header('Content-type', 'image/jpeg')
                        self.send_header('Content-length', str(len(jpegImage.getData())))
                        self.end_headers()
                        self.wfile.write(jpegImage.getData())
                        self.end_headers()

                        fpsCounter = fpsCounter + 1
                        if time.time() - timeCounter > 1:
                            node.warn(f'FPS: {fpsCounter}')
                            fpsCounter = 0
                            timeCounter = time.time()
                except Exception as ex:
                    node.warn(str(ex))

    with ThreadingSimpleServer(("", PORT), HTTPHandler) as httpd:
        node.warn(f"Serving at {get_ip_address('re0')}:{PORT}")
        #httpd.serve_forever()
        while True:
            httpd.handle_request()
            jpegImage = node.io['jpeg'].get()
""")

# Linking
xoutRgb.input.setBlocking(False)
jpeg.input.setBlocking(False)
script.inputs['jpeg'].setBlocking(False)

camRgb.video.link(xoutRgb.input)
camRgb.video.link(jpeg.input)
jpeg.bitstream.link(script.inputs['jpeg'])


# Connect to device and start pipeline
device = dai.Device(pipeline)
qRgb = device.getOutputQueue(name="rgb", maxSize=1, blocking=False)
print("Finished setting up cam!")

### Setup MQTT
def on_connect(client, userdata, flags, rc):
    print("Connected with:", rc)
    client.subscribe(triggerTopic) # Subscribe to the topic "rpi1/number"

def on_message(client, userdata, msg):
    print("Got msg", msg)
    inRgb = qRgb.get()
    print("Got image")
    cv2.imwrite(str(time.time()) + ".png", inRgb.getCvFrame())
    print("Saved image")
    client.publish(responseTopic, "Saved Image!")

client = mqtt.Client() # Create client object
client.on_connect = on_connect # link callback function to event
client.on_message = on_message # link callback function to event
client.connect(brokerID, brokerPort) # Connect to broker ("test.mosquitto.org" is a public testing broker)

client.loop_forever()